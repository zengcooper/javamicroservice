FROM java:8

WORKDIR /app

COPY target/*.jar /app

EXPOSE 8080

CMD java -jar /app/*.jar
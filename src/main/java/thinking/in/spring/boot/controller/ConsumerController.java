package thinking.in.spring.boot.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import thinking.in.spring.boot.entity.QueryMonthBill;
import thinking.in.spring.boot.entity.json.Response;
import thinking.in.spring.boot.service.ConsumerService;
import thinking.in.spring.boot.util.PageUtil;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/consumer")
@Slf4j
public class ConsumerController {
    @Autowired
    private ConsumerService consumerService;

    @RequestMapping(value = "/queryTotalAmount", method = RequestMethod.GET)
    public Response queryTotalAmount(@RequestParam("type") int type) {
        Response response = new Response();
        List list = new ArrayList();
        try {
            list = consumerService.queryTotalAmount(type);
        } catch (Exception e) {
            log.error("queryTotalAmount failed", e);
            return response.failure("server is busy,try again later");
        }
        response.setData(list);
        response.success("queryTotalAmount successfully");
        return response;
    }

    @RequestMapping(value = "/queryBill", method = RequestMethod.GET)
    public Response queryBill(@RequestParam("type") int type) {
        Response response = new Response();
        List<Map<String, Object>> list = null;
        try {
            list = consumerService.queryBill(type);
        } catch (Exception e) {
            log.error("queryBill failed", e);
            return response.failure("server is busy,try again later");
        }
        response.setData(list);
        response.success("queryBill successfully");
        return response;
    }

    @RequestMapping(value = "/queryHistoryBill", method = RequestMethod.GET)
    public Response queryHistoryBill() {
        Response response = new Response();
        List list = null;
        try {
            list = consumerService.queryHistoryBill();
        } catch (Exception e) {
            log.error("queryHistoryBill failed", e);
            return response.failure("server is busy,try again later");
        }
        response.setData(list);
        response.success("queryHistoryBill successfully");
        return response;
    }

    @RequestMapping(value = "/queryMonthBill", method = RequestMethod.POST)

    public Response queryMonthBill(@RequestBody QueryMonthBill queryMonthBill) {
        Response response = new Response();
        List list = null;
        String date = queryMonthBill.getDate();
        List<String> typeList = queryMonthBill.getTypeList();
        String keyWord = queryMonthBill.getKeyWord();
        int pageNum = queryMonthBill.getPageNum();
        int pageSize = queryMonthBill.getPageSize();
        try {
            String startTime = "";
            String endTime = "";
            if (StringUtils.isNotBlank(date)) {
                String[] str = date.split("-");
                startTime = str[0];
                endTime = str[1];
            }
            list = consumerService.queryMonthBill(typeList, keyWord, startTime, endTime);
            int totalNum = list.size();
            response.setTotalNum(totalNum);
            list = PageUtil.startPage(list, pageNum, pageSize);
        } catch (Exception e) {
            log.error("queryMonthBill failed", e);
            return response.failure("server is busy,try again later");
        }

        response.setData(list);
        response.success("queryMonthBill successfully");
        return response;
    }

    @RequestMapping(value = "/queryDetail", method = RequestMethod.GET)
    public Response queryDetail(@RequestParam("tradeTime") @NotNull String tradeTime) {
        Response response = new Response();
        Object object = null;
        try {
            object = consumerService.queryDetail(tradeTime);
        } catch (Exception e) {
            log.error("queryDetail failed", e);
            return response.failure("server is busy,try again later");
        }
        response.setData(object);
        response.success("queryDetail successfully");
        return response;
    }

    @RequestMapping(value = "/queryQuota", method = RequestMethod.GET)
    public Response queryQuota() {
        Response response = new Response();
        List list = null;
        try {
            list = consumerService.queryQuota();
        } catch (Exception e) {
            log.error("queryQuota failed", e);
            return response.failure("server is busy,try again later");
        }
        response.setData(list);
        response.success("queryQuota successfully");
        return response;
    }

    @RequestMapping(value = "/queryRecords", method = RequestMethod.GET)
    public Response queryRecords() {
        Response response = new Response();
        List list = null;
        try {
            list = consumerService.queryRecords();
        } catch (Exception e) {
            log.error("queryRecords failed", e);
            return response.failure("server is busy,try again later");
        }
        response.setData(list);
        response.success("queryRecords successfully");
        return response;
    }

    @RequestMapping(value = "/recordJump", method = RequestMethod.GET)
    public Response recordJump() {
        Response response = new Response();
        List list = null;
        try {
            list = consumerService.recordJump();
            int totalNum = list.size() - 1;
            response.setTotalNum(totalNum);
            list = PageUtil.startPage(list, 1, 20);
        } catch (Exception e) {
            log.error("recordJump failed", e);
            return response.failure("server is busy,try again later");
        }

        response.setData(list);
        response.success("recordJump successfully");
        return response;
    }

    @RequestMapping(value = "/billSelect", method = RequestMethod.GET)
    public Response billSelect() {
        Response response = new Response();
        List list = null;
        try {
            list = consumerService.billSelect();
        } catch (Exception e) {
            log.error("billSelect failed", e);
            return response.failure("server is busy,try again later");
        }
        response.setData(list);
        response.success("billSelect successfully");
        return response;
    }

}

package thinking.in.spring.boot.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author misfit
 */
@Data
public class Bill {

    /**
     * 交易金额
     */
    private double amount;

    /**
     * 卡号
     */
    private int cardNum;

    /**
     * 交易地
     */
    private String tradePlace;

    /**
     * 交易类型
     */
    private int tradeType;

    /**
     * 交易商家
     */
    private String dealer;

    /**
     * 交易时间
     */
    private Date tradeTime;

    /**
     * 入账时间
     */
    private Date countTime;

    /**
     * 状态，1：已入账，0：未入账
     */
    private int status;

}

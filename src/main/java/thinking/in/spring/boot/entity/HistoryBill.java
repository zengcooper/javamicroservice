package thinking.in.spring.boot.entity;

import lombok.Data;

import java.util.List;

/**
 * @author mengy
 * @date : 10:31 2021/3/5
 */
@Data
public class HistoryBill {
    private List<Object> lastYear;

    private List<Object> nowYear;
}

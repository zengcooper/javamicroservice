package thinking.in.spring.boot.service;


import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import thinking.in.spring.boot.entity.HistoryBill;
import thinking.in.spring.boot.util.Constant;
import thinking.in.spring.boot.util.PageUtil;
import thinking.in.spring.boot.util.TimeUtils;
import thinking.in.spring.boot.util.Tools;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author misfit
 */
@Slf4j
@Service
public class ConsumerService {

    @Value("classpath:month.json")
    private Resource monthAmount;

    @Value("classpath:day.json")
    private Resource dayAmount;

    @Value("classpath:dayDetail.json")
    private Resource dayDetail;

    private double yetBill = 0.0;

    public JSONArray queryTotalAmount(int type) {
        JSONArray array = new JSONArray();
        try {

            //按月份
            if (Constant.MONTH == type) {
                String data = IOUtils.toString(monthAmount.getInputStream(), StandardCharsets.UTF_8);
                array = JSONArray.parseArray(data);
            }

            //按天
            if (Constant.DAY == type) {
                List dayList = TimeUtils.getAllDay(7);
                for (int i = 0; i < dayList.size(); i++) {
                    LocalDate day = TimeUtils.fromString2LocalDate(dayList.get(i).toString(), "yyyy-MM-dd");
                    JSONObject json = getDayBill(day);
                    if (json.size() == 0) {
                        json.put("day", day.getDayOfMonth());
                        json.put("amount", "0.00");
                    }
                    array.add(json);
                }
            }
        } catch (Exception e) {
            log.error("查询首页折线图异常：", e);
        }
        return array;
    }

    public JSONObject getDayBill(LocalDate day) {
        JSONObject json = new JSONObject();
        try {
            String data = IOUtils.toString(dayDetail.getInputStream(), StandardCharsets.UTF_8);
            JSONArray dayArray = JSONArray.parseArray(data);
            for (int j = 0; j < dayArray.size(); j++) {
                JSONObject obj = dayArray.getJSONObject(j);
                LocalDate tradeTime = TimeUtils.fromString2LocalDate(obj.getString("tradeTime"), "yyyy/MM/dd HH:mm:ss");
                if (day.isEqual(tradeTime)) {
                    json.put("day", day.getDayOfMonth());
                    json.put("amount", obj.getString("amount"));
                }
            }
        } catch (Exception e) {
            log.error("getDayBill error", e);
        }
        return json;
    }

    public List<Map<String, Object>> queryBill(int status) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> map = new HashMap();
        //获取当前时间
        int dayNow = TimeUtils.getDayOfMonth();
        int month = TimeUtils.getMonthValue();
        String startTime = "";
        String endTime = "";
        //本期账单
        if (Constant.RECORDED == status) {
            map.put("type", Constant.RECORDED);
            map.put("payDate", "0" + month + "/05");
            /**
             * 如果日期中的日小于等于20，本期账单n-2月21日到n-1月20日；
             * 如果日期大于20，显示n-1月21日到当月20日；
             */
            if (dayNow > Constant.TWENTY) {
                startTime = "0" + (month - 1) + "/21";
                endTime = "0" + month + "/20";
            } else {
                startTime = "0" + (month - 2) + "/21";
                endTime = "0" + (month - 1) + "/20";
            }
        } else {
            //未出账单
            map.put("type", Constant.UNRECORDED);
            map.put("payDate", "0" + (month + 1) + "/05");
            /**
             * 如果日期中的日小于等于20，本期账单n-1月21日到当月20日；
             * 如果日期大于20，显示当月21日到n+1月20日；
             */
            if (dayNow > Constant.TWENTY) {
                startTime = "0" + month + "/21";
                endTime = "0" + (month + 1) + "/20";
            } else {
                startTime = "0" + (month - 1) + "/21";
                endTime = "0" + month + "/20";
            }
        }
        map.put("startTime", ("2021/" + startTime));
        map.put("endTime", ("2021/" + endTime));

        double totalCount = 0.00;
        String data = IOUtils.toString(dayDetail.getInputStream(), StandardCharsets.UTF_8);
        JSONArray array = JSONArray.parseArray(data);

        LocalDateTime end = TimeUtils.fromString2LocalDateTime("2021/".concat(endTime).concat(" 23:59:59"), "yyyy/MM/dd HH:mm:ss");
        LocalDateTime start = TimeUtils.fromString2LocalDateTime("2021/".concat(startTime).concat(" 00:00:00"), "yyyy/MM/dd HH:mm:ss");
        for (int i = 0; i < array.size(); i++) {
            JSONObject object = array.getJSONObject(i);
            String dateT = object.getString("tradeTime");
            LocalDateTime dateL = TimeUtils.fromString2LocalDateTime(dateT, "yyyy/MM/dd HH:mm:ss");
            if (dateL.isBefore(end) && dateL.isAfter(start)) {
                double single = Double.parseDouble(object.getString("amount"));
                totalCount = Tools.sum(totalCount, single);
            }
        }
        if (Constant.UNRECORDED == status) {
            yetBill = totalCount;
        }
        double minAmount = Tools.round(Tools.mul(totalCount, 0.1), 2);
        totalCount = Tools.round(totalCount, 2);
        map.put("minAmount", Tools.formatDouble(minAmount));
        map.put("totalAmount", Tools.formatDouble(totalCount));

        list.add(map);
        return list;
    }

    public List<Object> queryHistoryBill() {
        List<Object> result = new ArrayList();
        HistoryBill historyBill = new HistoryBill();
        LocalDateTime cur = LocalDateTime.now();
        LocalDateTime end = TimeUtils.fromString2LocalDateTime("2021/03/20 00:00:00", "yyyy/MM/dd HH:mm:ss");
        JSONObject obj = new JSONObject();
        obj.put("month", "3月账单");
        obj.put("date", "2021/02/21-2021/03/20");
        obj.put("totalAmount", 16397.27);

        JSONObject obj1 = new JSONObject();
        obj1.put("month", "2月账单");
        obj1.put("date", "2021/01/21-2021/02/20");
        obj1.put("totalAmount", 11032.58);

        JSONObject obj2 = new JSONObject();
        obj2.put("month", "1月账单");
        obj2.put("date", "2020/12/21-2021/01/20");
        obj2.put("totalAmount", 28687.10);

        List<Object> now = new ArrayList();
        if (cur.isAfter(end)) {
            now.add(obj);
        }
        now.add(obj1);
        now.add(obj2);
        historyBill.setNowYear(now);

        JSONObject obj3 = new JSONObject();
        obj3.put("month", "12月账单");
        obj3.put("date", "2020/11/21-2020/12/20");
        obj3.put("totalAmount", 400.00);

        JSONObject obj4 = new JSONObject();
        obj4.put("month", "11月账单");
        obj4.put("date", "2020/10/21-2020/11/20");
        obj4.put("totalAmount", 5000.00);

        JSONObject obj5 = new JSONObject();
        obj5.put("month", "10月账单");
        obj5.put("date", "2020/09/21-2020/10/20");
        obj5.put("totalAmount", 4400.00);

        JSONObject obj6 = new JSONObject();
        obj6.put("month", "9月账单");
        obj6.put("date", "2020/08/21-2020/09/20");
        obj6.put("totalAmount", 4900.00);

        JSONObject obj7 = new JSONObject();
        obj7.put("month", "8月账单");
        obj7.put("date", "2020/07/21-2020/08/20");
        obj7.put("totalAmount", 8900.00);

        JSONObject obj8 = new JSONObject();
        obj8.put("month", "7月账单");
        obj8.put("date", "2020/06/21-2020/07/20");
        obj8.put("totalAmount", 23900.00);

        JSONObject obj9 = new JSONObject();
        obj9.put("month", "6月账单");
        obj9.put("date", "2020/05/21-2020/06/20");
        obj9.put("totalAmount", 33600.00);
        List<Object> last = new ArrayList();
        last.add(obj3);
        last.add(obj4);
        last.add(obj5);
        last.add(obj6);
        last.add(obj7);
        last.add(obj8);
        last.add(obj9);
        historyBill.setLastYear(last);
        result.add(historyBill);
        return result;
    }

    public List<Object> queryMonthBill(List<String> typeList, String keyWord, String startTime, String endTime) {
        List<Object> list = new ArrayList();
        try {
            String dataAll = IOUtils.toString(dayDetail.getInputStream(), StandardCharsets.UTF_8);
            JSONArray array = JSONArray.parseArray(dataAll);

            for (int i = 0; i < array.size(); i++) {
                JSONObject object = array.getJSONObject(i);
                //账单过滤
                if (CollectionUtil.isNotEmpty(typeList)) {

                    if (typeList.contains(object.getString("type")) && object.getString("dealer").contains(keyWord)) {
                        list.add(object);
                    }
                } else {
                    //账期过滤
                    String dateT = object.getString("tradeTime");
                    LocalDateTime end = TimeUtils.fromString2LocalDateTime(endTime.concat(" 23:59:59"), "yyyy/MM/dd HH:mm:ss");

                    LocalDateTime start = TimeUtils.fromString2LocalDateTime(startTime.concat(" 00:00:00"), "yyyy/MM/dd HH:mm:ss");
                    LocalDateTime dateL = TimeUtils.fromString2LocalDateTime(dateT, "yyyy/MM/dd HH:mm:ss");
                    if (dateL.isBefore(end) && dateL.isAfter(start) && object.getString("dealer").contains(keyWord)) {
                        list.add(object);
                    }
                }
            }
        } catch (Exception e) {
            log.error("查询每月账单异常", e);
        }
        return list;
    }

    public Object queryDetail(String tradeTime) {
        JSONObject object = null;
        try {
            String dataAll = IOUtils.toString(dayDetail.getInputStream(), StandardCharsets.UTF_8);
            JSONArray array = JSONArray.parseArray(dataAll);
            for (int i = 0; i < array.size(); i++) {
                JSONObject json = array.getJSONObject(i);
                if (tradeTime.equals(json.getString("tradeTime"))) {
                    object = json;
                    break;
                }
            }
        } catch (Exception e) {
            log.error("查询交易明细异常", e);
        }
        return object;
    }

    public List<Object> queryQuota() {
        List<Object> list = new ArrayList();
        try {
            queryBill(0);
            int month = TimeUtils.getMonthValue();
            JSONObject json = new JSONObject();
            //固定额度
            json.put("fixedQuota", "60000.00");
            //临时额度
            json.put("tempQuota", "16000.00");
            //已用额度
            json.put("usedQuota", Tools.formatDouble(yetBill));
            //可用额度
            double avl = Tools.sub(76000.00, yetBill);
            json.put("avlQuota", Tools.formatDouble(avl));
            //总额度
            json.put("totalQuota", "76000.00");
            //到期时间
            json.put("date", TimeUtils.getDayPlus(15));
            list.add(json);
        } catch (Exception e) {
            log.error("查询额度异常", e);
        }
        return list;
    }

    public List<Object> queryRecords() {
        List<Object> list = new ArrayList();
        try {
            String dataAll = IOUtils.toString(dayDetail.getInputStream(), StandardCharsets.UTF_8);
            JSONArray array = JSONArray.parseArray(dataAll);
            for (int i = 0; i < 10; i++) {
                list.add(array.get(i));
            }
        } catch (Exception e) {
            log.error("查询交易记录失败", e);
        }
        return list;
    }


    public List<Object> billSelect() {
        List<Object> result = new ArrayList();
        //获取当前时间
        int dayNow = TimeUtils.getDayOfMonth();
        int month = TimeUtils.getMonthValue();
        //本期日期
        String startTimeCur = "";
        String endTimeCur = "";
        if (dayNow > Constant.TWENTY) {
            startTimeCur = "0" + (month - 1) + "/21";
            endTimeCur = "0" + month + "/20";
        } else {
            startTimeCur = "0" + (month - 2) + "/21";
            endTimeCur = "0" + (month - 1) + "/20";
        }
        JSONObject current = new JSONObject();
        current.put("name", "本期");
        current.put("flag", false);
        current.put("date", ("2021/" + startTimeCur + "-" + "2021/" + endTimeCur));

        //未出
        String startTimeYet = "";
        String endTimeCurYet = "";
        JSONObject yet = new JSONObject();
        yet.put("name", "未出");
        if (dayNow > Constant.TWENTY) {
            startTimeYet = "0" + month + "/21";
            endTimeCurYet = "0" + (month + 1) + "/20";
        } else {
            startTimeYet = "0" + (month - 1) + "/21";
            endTimeCurYet = "0" + month + "/20";
        }
        yet.put("date", ("2021/" + startTimeYet + "-" + "2021/" + endTimeCurYet));
        yet.put("flag", true);

        JSONObject month1 = new JSONObject();
        month1.put("name", "1月");
        month1.put("date", "2020/12/21-2021/01/20");
        month1.put("flag", false);

        JSONObject month12 = new JSONObject();
        month12.put("name", "12月");
        month12.put("date", "2020/11/21-2020/12/20");
        month12.put("flag", false);

        JSONObject month11 = new JSONObject();
        month11.put("name", "11月");
        month11.put("date", "2020/10/21-2020/11/20");
        month11.put("flag", false);

        JSONObject month10 = new JSONObject();
        month10.put("name", "10月");
        month10.put("date", "2020/09/21-2020/10/20");
        month10.put("flag", false);

        JSONObject month9 = new JSONObject();
        month9.put("name", "9月");
        month9.put("date", "2020/08/21-2020/09/20");
        month9.put("flag", false);

        JSONObject month8 = new JSONObject();
        month8.put("name", "8月");
        month8.put("date", "2020/07/21-2020/08/20");
        month8.put("flag", false);

        JSONObject month7 = new JSONObject();
        month7.put("name", "7月");
        month7.put("date", "2020/06/21-2020/07/20");
        month7.put("flag", false);

        JSONObject month6 = new JSONObject();
        month6.put("name", "6月");
        month6.put("date", "2020/05/21-2020/06/20");
        month6.put("flag", false);

        JSONObject month5 = new JSONObject();
        month5.put("name", "5月");
        month5.put("date", "2020/04/21-2020/05/20");
        month5.put("flag", false);

        JSONObject month4 = new JSONObject();
        month4.put("name", "4月");
        month4.put("date", "2020/03/21-2020/04/20");
        month4.put("flag", false);

        JSONArray arr = new JSONArray();
        JSONArray array = new JSONArray();
        array.add(month12);
        array.add(month11);
        array.add(month10);
        array.add(month9);
        array.add(month8);
        array.add(month7);
        array.add(month6);
        array.add(month5);
        array.add(month4);
        arr.add(current);
        arr.add(yet);
        arr.add(month1);
        JSONObject year = new JSONObject();
        year.put("2020年账单", array);
        year.put("2021年账单", arr);
        result.add(year);
        return result;
    }


    /**
     * 更多跳转账单明细
     *
     * @return
     */
    public List<Object> recordJump() {
        List<Object> resultList = new ArrayList();
        String startTime = "2021/02/21";
        String endTime = "2021/03/20";
        JSONObject title = new JSONObject();
        title.put("title", "未出账单");
        resultList.add(title);
        resultList.addAll(queryMonthBill(null, "", startTime, endTime));
        return resultList;
    }

}

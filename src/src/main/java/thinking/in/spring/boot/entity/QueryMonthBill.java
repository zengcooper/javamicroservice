package thinking.in.spring.boot.entity;

import lombok.Data;

import java.util.List;

/**
 * @author mengy
 * @date : 15:27 2021/3/5
 */
@Data
public class QueryMonthBill {

    private String date;

    private List<String> typeList;

    private String keyWord;

    private int pageNum;

    private int pageSize;


}

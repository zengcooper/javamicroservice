package thinking.in.spring.boot.util;

/**
 * @author mengy
 * @date : 15:51 2021/2/25
 */
public class Constant {
    //利息
    public static final Integer TRADE_TYPE_1 = 1;
    //消费
    public static final Integer TRADE_TYPE_2 = 2;
    //还款
    public static final Integer TRADE_TYPE_3 = 3;
    //费用
    public static final Integer TRADE_TYPE_4 = 4;
    //分期
    public static final Integer TRADE_TYPE_5 = 5;
    //其他
    public static final Integer TRADE_TYPE_6 = 6;

    //已入账
    public static final Integer RECORDED = 1;
    //未入账
    public static final Integer UNRECORDED = 0;

    public static final int MONTH = 1;//月

    public static final int DAY = 2;//日

    public static final int TWENTY = 20;
}

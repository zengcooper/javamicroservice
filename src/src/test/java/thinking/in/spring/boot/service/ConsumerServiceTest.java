package thinking.in.spring.boot.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;


/**
 * @author mengy
 * @date : 11:15 2021/3/8
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ConsumerService.class})
class ConsumerServiceTest {
    @Resource
    private ConsumerService consumerService;

    @Test
    void queryTotalAmount() {
        assert consumerService.queryTotalAmount(1).size() == 7;
        assert consumerService.queryTotalAmount(2).size() == 7;
    }

    @Test
    void queryBill() throws IOException {
        assert consumerService.queryBill(1).size() > 0;
        assert consumerService.queryBill(0).size() > 0;
    }

    @Test
    void queryHistoryBill() {
        assert consumerService.queryHistoryBill().size() > 0;
    }

    @Test
    void queryMonthBill() {
        assert consumerService.queryMonthBill(null, "", "02/21", "03/20",1,20).size() > 0;
    }

    @Test
    void queryDetail() {

        assert consumerService.queryDetail("2021/02/21 12:00:00") != null;
    }

    @Test
    void queryQuota() {
        assert consumerService.queryQuota().size() > 0;
    }

    @Test
    void queryRecords() {
        assert consumerService.queryRecords().size() > 0;
    }

    @Test
    void billSelect() {
        assert consumerService.billSelect().size() > 0;
    }

    @Test
    void recordJump() {
        assert consumerService.recordJump().size() > 0;
    }
}